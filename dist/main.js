/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/js/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/js/algo/moveSeeds.js":
/*!**********************************!*\
  !*** ./src/js/algo/moveSeeds.js ***!
  \**********************************/
/*! exports provided: createReserve, emptyStartHole, scatter, takeSeeds, animGive, animTake */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"createReserve\", function() { return createReserve; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"emptyStartHole\", function() { return emptyStartHole; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"scatter\", function() { return scatter; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"takeSeeds\", function() { return takeSeeds; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"animGive\", function() { return animGive; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"animTake\", function() { return animTake; });\n\n\n\nfunction createReserve(seedsArray, indexOfClick) {\n    // prend en paramètres une liste du nombre de graines par trou et l'index du clic sur le tableau (le trou de départ)\n    // return le nb de graines du trou\n    \n    let reserve = seedsArray[indexOfClick]\n    return reserve;\n}\n\nfunction emptyStartHole(seedsArray, indexOfClick) {\n    // prend en paramètres une liste du nombre de graines par trou et l'index du clic sur le tableau (le trou de départ)\n    // passe la valeur du tableau seedsArray à l'indice indexOfClick à 0\n    // return seedsArray après avoir vidé le trou de départ de toutes ses graines (après appel de createReserve(...);)\n    \n    seedsArray[indexOfClick] = 0\n    \n    return seedsArray;\n}\n\nfunction scatter(seedsArray, reserve, indexOfClick, HTMLColl) {\n    // prend en paramètre une liste du nombre de graines par trou, la réserve correspondant au nombre de graines\n    // récoltées dans le trou sur le clic et l'index du clic sur le tableau (correspondant au trou cliqué)\n    // disperse les graines sur les trous suivant à raison de une graine par trou dans le sens de jeu\n    // jusqu'à ce que la réserve soit vide (on ne sème pas de graine dans le trou de départ)\n    \n    let i = indexOfClick\n    let reserveLocale = reserve\n    \n    while (reserveLocale > 0) {\n        \n        if (i != indexOfClick) {\n            seedsArray[i] += 1\n            reserveLocale -= 1\n            animGive(HTMLColl[i]);  /*                  ajouter fonction animGive qui prendra un elem d'collection HTML en par                  */\n        }\n        \n        i += 1\n        \n        if (i >= seedsArray.length) {\n            i = 0\n        }\n    }\n    return seedsArray;    // devra retourner lastSeedIndex mais aussi mettre à jour le tableau\n}\n\n\nfunction takeSeeds(lastIndex, anArray, player, HTMLColl) {\n    // parameters: lastIndex -> the last seed's position in the array \n    //             anArray -> the array containing the number of seeds in each hole\n    // returns the array to update ??? (use global variable??)\n    \n    let localArray = anArray\n    localArray.reverse();\n    let localIndex = Math.abs(lastIndex - 11)\n    let localGrenier = 0\n    \n    for (let i = localIndex; i < localArray.length; i++) {\n\n        if ((player == \"top\" && i >= 6) || (player == \"bottom\" && i <= 5)) {\n            break;\n        }\n\n        if (localArray[i] == 2 || localArray[i] == 3) {\n            localGrenier += localArray[i]\n            localArray[i] = 0\n            // if (player == \"top\") {\n            //     animTake(HTMLColl[i]);\n            // } else {\n            //     animTake(HTMLColl[i]);\n            // }\n               /*                  ajouter fonction animTake qui prendra un elem d'une collection HTML en par                  */\n        } else {\n            break;\n        }\n    }\n    localArray.reverse();\n    return [localArray, localGrenier];\n}\n\nfunction animGive(DOMelem) {\n    DOMelem.classList.add('animGive');\n    \n    setTimeout(() => {\n        DOMelem.classList.remove('animGive');\n    }, 1500);    // en fonction de la durée de l'anim pour l'instant 1.5sec\n}\n\nfunction animTake(DOMelem) {\n    DOMelem.classList.add('animTake');\n    setTimeout(() => {\n        DOMelem.classList.remove('animTake');\n    }, 1500);    // en fonction de la durée de l'anim pour l'instant 1.5sec\n}\n\n//# sourceURL=webpack:///./src/js/algo/moveSeeds.js?");

/***/ }),

/***/ "./src/js/algo/validations.js":
/*!************************************!*\
  !*** ./src/js/algo/validations.js ***!
  \************************************/
/*! exports provided: authorizedSide, nonEmptyHole, onOpponentsHole, allOpponentsEmpty, canGive, canGiveFullTest, winner, winnerByDefault */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"authorizedSide\", function() { return authorizedSide; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"nonEmptyHole\", function() { return nonEmptyHole; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"onOpponentsHole\", function() { return onOpponentsHole; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"allOpponentsEmpty\", function() { return allOpponentsEmpty; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"canGive\", function() { return canGive; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"canGiveFullTest\", function() { return canGiveFullTest; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"winner\", function() { return winner; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"winnerByDefault\", function() { return winnerByDefault; });\n\nfunction authorizedSide(clicIndex, player) {\n    // prend en paramètre un index -> indique sur quel trou on a cliqué\n    // et player -> joueur du plateau top ou bottom donc valeurs \"top\" ou \"bottom\"\n    // return un boolean, true si le joueur clic dans son camp, false sinon\n\n    let authorized = false\n    if ((player == \"top\") && (clicIndex < 6)) {\n        authorized = true;\n    } else if ((player == \"bottom\") && (clicIndex >= 6)){\n        authorized = true;\n    }\n\n    return authorized;\n}\nfunction nonEmptyHole(anArray, clicIndex) {\n    // prend en paramètres une liste -> le nb de graines par trou\n    // et un index -> indique sur quel trou on a cliqué\n    // return un boolean, true si le trou sous le clic a des graines, false sinon\n\n    let authorized = false\n    let seedsOnClic = anArray[clicIndex]\n    if (seedsOnClic > 0) {\n        authorized = true\n    }\n\n    return authorized;\n}\nfunction onOpponentsHole(player, lastSeedIndex) {\n    // parameters: a string, player indicates who is the player,\n    // an index -> indicates in which hole the player scattered his last seed\n    // returns a boolean, true if we are on the opponents hole\n    \n    let authorized = false\n    if (player == \"top\" && lastSeedIndex > 5) {\n        authorized = true\n    } else if (player == \"bottom\" && lastSeedIndex < 6) {\n        authorized = true\n    }\n    return authorized;\n}\n\n\nfunction allOpponentsEmpty(anArray, player) {\n\n    let theyReEmpty = true\n    anArray.forEach(function (element, index) {\n        if (onOpponentsHole(player, index) && element != 0) {\n            theyReEmpty = false\n            return theyReEmpty;\n        }\n    });\n    return theyReEmpty;\n}\n\nfunction canGive(anElement, anIndex) {\n\n    let toOpponentsSide = 6 - (anIndex % 6)\n    let possible = false\n\n    if (anElement >= toOpponentsSide) {\n        possible = true\n    }\n    return possible\n}\n\nfunction canGiveFullTest(anArray, player) {\n    let possible = true\n    let possibleIndicator = 0\n\n    anArray.forEach(function (element, index) {\n        if ((player == \"top\" && index <= 5) || (player == \"bottom\" && index >= 6)) {\n\n            if (!canGive(element, index)) {\n                possibleIndicator += 1\n            }\n        }\n    });\n    if (possibleIndicator == 6) {\n        possible = false\n    }\n    return possible\n}\n\n\nfunction winner(player) {\n    // returns the winner \"top\" or \"bottom\" or \"none\" if no one won yet\n\n    let reserveTop = document.getElementById('top').innerHTML;\n    let reserveBottom = document.getElementById('bottom').innerHTML;\n    let winnerIs = \"none\"\n\n    reserveTop = parseInt(reserveTop);\n    reserveBottom = parseInt(reserveBottom);\n\n    if (reserveTop > 24) {\n        winnnerIs = \"top\"\n    } else if (reserveBottom > 24) {\n        winnerIs = \"bottom\"\n    }\n    return winnerIs;\n}\n\nfunction winnerByDefault(player) {\n    let reserveTop = document.getElementById('top');\n    let reserveBottom = document.getElementById('bottom');\n    let winnerIs = \"none\"\n\n    reserveTop = parseInt(reserveTop);\n    reserveBottom = parseInt(reserveBottom);\n\n    if (reserveTop > reserveBottom) {\n        winnerIs = \"top\"\n    } else if (reserveBottom > reserveTop) {\n        winnerIs = \"bottom\"\n    }\n    return winnerIs;\n}\n\n//# sourceURL=webpack:///./src/js/algo/validations.js?");

/***/ }),

/***/ "./src/js/dom/getFromDOM.js":
/*!**********************************!*\
  !*** ./src/js/dom/getFromDOM.js ***!
  \**********************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _updateDOM_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./updateDOM.js */ \"./src/js/dom/updateDOM.js\");\n/* harmony import */ var _algo_validations_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../algo/validations.js */ \"./src/js/algo/validations.js\");\n/* harmony import */ var _algo_moveSeeds__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../algo/moveSeeds */ \"./src/js/algo/moveSeeds.js\");\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\nfunction playerNamebot() { //nom joueur 1\n  var name1 = prompt(\"Nom joueur 1\");\n  document.getElementById('namebot').innerHTML = \"joueur 1 :\" + name1;\n}\nfunction playerNametop() {\n  var name2 = prompt(\"Nom joueur 2\"); //nom joueur 2\n  document.getElementById('nametop').innerHTML = \"joueur 2 :\" + name2;\n}\nplayerNamebot();\nplayerNametop();\nfunction seedsAr() {\n  // return a table containing the number of seeds in each hole,\n  // the index of the table matches the playing direction\n  let holes = document.getElementsByClassName('hole')\n  let tabSeeds = []\n  for(let i=0; i < holes.length; i++) {\n    let seeds = holes[i].getAttribute('data-nbSeed')\n    seeds = parseInt(seeds);\n    tabSeeds.push(seeds);\n  }\n  return tabSeeds\n}\nlet theIndexOne = 0\nlet daPlayer = \"top\"\n\n\n\n\nconst bigboardgame = document.querySelector('.boardgame');\n\nbigboardgame.addEventListener('click', ({target}) => {\n  if (!target.matches('.hole')) return;\n  \n  let holes = document.getElementsByClassName('hole');\n  holes = Array.from(holes);\n  theIndexOne = holes.indexOf(target);\n  \n  let holesHTMLColl = document.getElementsByClassName('hole');\n  if (!Object(_algo_validations_js__WEBPACK_IMPORTED_MODULE_1__[\"authorizedSide\"])(theIndexOne, daPlayer)) {\n    Object(_algo_moveSeeds__WEBPACK_IMPORTED_MODULE_2__[\"animTake\"])(holesHTMLColl[theIndexOne]);\n    return\n  }\n  let theArray = seedsAr()\n  \n  if (!Object(_algo_validations_js__WEBPACK_IMPORTED_MODULE_1__[\"nonEmptyHole\"])(theArray, theIndexOne)) {\n    return\n  }\n  if (Object(_algo_validations_js__WEBPACK_IMPORTED_MODULE_1__[\"allOpponentsEmpty\"])(theArray, daPlayer)) {\n    if (Object(_algo_validations_js__WEBPACK_IMPORTED_MODULE_1__[\"canGiveFullTest\"])(theArray, daPlayer))   {\n      if (!Object(_algo_validations_js__WEBPACK_IMPORTED_MODULE_1__[\"canGive\"])(theArray[theIndexOne], theIndexOne)) {\n        return\n      }\n    } else {\n      if (Object(_algo_validations_js__WEBPACK_IMPORTED_MODULE_1__[\"winnerByDefault\"])(daPlayer) == \"top\") {\n        alert('Top player wins !');\n      } else {\n        alert('Bottom player wins');\n      }\n    }\n  }\n  \n  let theReserve = Object(_algo_moveSeeds__WEBPACK_IMPORTED_MODULE_2__[\"createReserve\"])(theArray, theIndexOne);\n  theArray = Object(_algo_moveSeeds__WEBPACK_IMPORTED_MODULE_2__[\"emptyStartHole\"])(theArray, theIndexOne);\n  \n  let theLastIndex = (theIndexOne + theReserve) % 12\n  \n  theArray = Object(_algo_moveSeeds__WEBPACK_IMPORTED_MODULE_2__[\"scatter\"])(theArray, theReserve, theIndexOne, holesHTMLColl);\n  \n\n  Object(_updateDOM_js__WEBPACK_IMPORTED_MODULE_0__[\"nbSeedUpdate\"])(theArray);\n  \n  let array2elems = Object(_algo_moveSeeds__WEBPACK_IMPORTED_MODULE_2__[\"takeSeeds\"])(theLastIndex, theArray, daPlayer, holesHTMLColl);\n  theArray = array2elems[0]\n  let grenier = array2elems[1]\n  Object(_updateDOM_js__WEBPACK_IMPORTED_MODULE_0__[\"nbSeedUpdate\"])(theArray);\n\n  Object(_updateDOM_js__WEBPACK_IMPORTED_MODULE_0__[\"reserveUpdate\"])(grenier, daPlayer);\n\n  if (Object(_algo_validations_js__WEBPACK_IMPORTED_MODULE_1__[\"winner\"])(daPlayer) == \"top\") {\n    alert('Top player wins !');\n  } else if (Object(_algo_validations_js__WEBPACK_IMPORTED_MODULE_1__[\"winner\"])(daPlayer) == \"bottom\") {\n    alert('Bottom player wins !');\n  }\n\n  if (daPlayer == \"top\") {\n    daPlayer = \"bottom\"\n  } else {\n    daPlayer = \"top\"\n  }\n  Object(_updateDOM_js__WEBPACK_IMPORTED_MODULE_0__[\"swapCursor\"])(daPlayer);\n  Object(_updateDOM_js__WEBPACK_IMPORTED_MODULE_0__[\"swapColor\"])(daPlayer);\n  return\n});\n\n//# sourceURL=webpack:///./src/js/dom/getFromDOM.js?");

/***/ }),

/***/ "./src/js/dom/updateDOM.js":
/*!*********************************!*\
  !*** ./src/js/dom/updateDOM.js ***!
  \*********************************/
/*! exports provided: nbSeedUpdate, swapColor, swapCursor, grenierUpdate, reserveUpdate */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"nbSeedUpdate\", function() { return nbSeedUpdate; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"swapColor\", function() { return swapColor; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"swapCursor\", function() { return swapCursor; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"grenierUpdate\", function() { return grenierUpdate; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"reserveUpdate\", function() { return reserveUpdate; });\nfunction nbSeedUpdate(anArray) {\n    let elemAr = document.getElementsByClassName('hole');\n    for (let i = 0; i < elemAr.length; i++) {\n        elemAr[i].setAttribute(\"data-nbSeed\", `${anArray[i]}`);\n        elemAr[i].innerHTML = `${anArray[i]}`\n    }\n}\nfunction swapColor(player) { //changement de couleur\n    let play1 = document.getElementById('botQ');\n    let play2 = document.getElementById('topQ');\n\n    if (player == \"top\") {\n        play2.style.backgroundColor = \"red\";\n        play1.style.backgroundColor = \"#212121\";\n    } else {\n        \n        play2.style.backgroundColor = \"#212121\";\n        play1.style.backgroundColor = \"blue\";\n    }\n}\nfunction swapCursor(player) {\n    let play1 = document.getElementById('player1b')\n    let play2 = document.getElementById('player2b')\n\n    if (player == \"top\") {\n        play2.style.cursor = \"grab\";\n        play1.style.cursor = \"not-allowed\";\n    }\n    else {\n        play2.style.cursor = \"not-allowed\";\n        play1.style.cursor = \"grab\";\n    }\n}\nfunction grenierUpdate(grenier, player) {\n    let localGrenier = grenier\n    if (player == \"top\") {\n        let player2 = document.getElementById('top');\n        console.log(player2);\n        let player2Grenier = player2.innerHTML\n        player2Grenier = parseInt(player2Grenier);\n        player2Grenier += grenier\n        player2Grenier.toString\n        player2.innerHTML = player2Grenier\n    } else {\n        let player1 = document.getElementById('bottom');\n        let player1Grenier = player1.innerHTML\n        player1Grenier = parseInt(player1Grenier);\n        player1Grenier += grenier\n        player1Grenier.toString\n        player1.innerHTML = player2Grenier\n    }\n}\n\nfunction reserveUpdate(reserve, player) {\n\n    let playersDiv = document.getElementById(player)\n    let localReserve = playersDiv.innerHTML\n    let toAdd = reserve\n    \n\n    localReserve = parseInt(localReserve);\n    localReserve += toAdd\n    playersDiv.innerHTML = localReserve\n\n}\n\n//# sourceURL=webpack:///./src/js/dom/updateDOM.js?");

/***/ }),

/***/ "./src/js/index.js":
/*!*************************!*\
  !*** ./src/js/index.js ***!
  \*************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _dom_updateDOM_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./dom/updateDOM.js */ \"./src/js/dom/updateDOM.js\");\n/* harmony import */ var _dom_getFromDOM_js__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./dom/getFromDOM.js */ \"./src/js/dom/getFromDOM.js\");\n\n\n\n\n//# sourceURL=webpack:///./src/js/index.js?");

/***/ })

/******/ });