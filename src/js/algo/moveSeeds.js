


export function createReserve(seedsArray, indexOfClick) {
    // prend en paramètres une liste du nombre de graines par trou et l'index du clic sur le tableau (le trou de départ)
    // return le nb de graines du trou
    
    let reserve = seedsArray[indexOfClick]
    return reserve;
}

export function emptyStartHole(seedsArray, indexOfClick) {
    // prend en paramètres une liste du nombre de graines par trou et l'index du clic sur le tableau (le trou de départ)
    // passe la valeur du tableau seedsArray à l'indice indexOfClick à 0
    // return seedsArray après avoir vidé le trou de départ de toutes ses graines (après appel de createReserve(...);)
    
    seedsArray[indexOfClick] = 0
    
    return seedsArray;
}

export function scatter(seedsArray, reserve, indexOfClick, HTMLColl) {
    // prend en paramètre une liste du nombre de graines par trou, la réserve correspondant au nombre de graines
    // récoltées dans le trou sur le clic et l'index du clic sur le tableau (correspondant au trou cliqué)
    // disperse les graines sur les trous suivant à raison de une graine par trou dans le sens de jeu
    // jusqu'à ce que la réserve soit vide (on ne sème pas de graine dans le trou de départ)
    
    let i = indexOfClick
    let reserveLocale = reserve
    
    while (reserveLocale > 0) {
        
        if (i != indexOfClick) {
            seedsArray[i] += 1
            reserveLocale -= 1
            animGive(HTMLColl[i]);  /*                  ajouter fonction animGive qui prendra un elem d'collection HTML en par                  */
        }
        
        i += 1
        
        if (i >= seedsArray.length) {
            i = 0
        }
    }
    return seedsArray;    // devra retourner lastSeedIndex mais aussi mettre à jour le tableau
}


export function takeSeeds(lastIndex, anArray, player, HTMLColl) {
    // parameters: lastIndex -> the last seed's position in the array 
    //             anArray -> the array containing the number of seeds in each hole
    // returns the array to update ??? (use global variable??)
    
    let localArray = anArray
    localArray.reverse();
    let localIndex = Math.abs(lastIndex - 11)
    let localGrenier = 0
    
    for (let i = localIndex; i < localArray.length; i++) {

        if ((player == "top" && i >= 6) || (player == "bottom" && i <= 5)) {
            break;
        }

        if (localArray[i] == 2 || localArray[i] == 3) {
            localGrenier += localArray[i]
            localArray[i] = 0
            // if (player == "top") {
            //     animTake(HTMLColl[i]);
            // } else {
            //     animTake(HTMLColl[i]);
            // }
               /*                  ajouter fonction animTake qui prendra un elem d'une collection HTML en par                  */
        } else {
            break;
        }
    }
    localArray.reverse();
    return [localArray, localGrenier];
}

export function animGive(DOMelem) {
    DOMelem.classList.add('animGive');
    
    setTimeout(() => {
        DOMelem.classList.remove('animGive');
    }, 1500);    // en fonction de la durée de l'anim pour l'instant 1.5sec
}

export function animTake(DOMelem) {
    DOMelem.classList.add('animTake');
    setTimeout(() => {
        DOMelem.classList.remove('animTake');
    }, 1500);    // en fonction de la durée de l'anim pour l'instant 1.5sec
}