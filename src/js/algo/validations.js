
export function authorizedSide(clicIndex, player) {
    // prend en paramètre un index -> indique sur quel trou on a cliqué
    // et player -> joueur du plateau top ou bottom donc valeurs "top" ou "bottom"
    // return un boolean, true si le joueur clic dans son camp, false sinon

    let authorized = false
    if ((player == "top") && (clicIndex < 6)) {
        authorized = true;
    } else if ((player == "bottom") && (clicIndex >= 6)){
        authorized = true;
    }

    return authorized;
}
export function nonEmptyHole(anArray, clicIndex) {
    // prend en paramètres une liste -> le nb de graines par trou
    // et un index -> indique sur quel trou on a cliqué
    // return un boolean, true si le trou sous le clic a des graines, false sinon

    let authorized = false
    let seedsOnClic = anArray[clicIndex]
    if (seedsOnClic > 0) {
        authorized = true
    }

    return authorized;
}
export function onOpponentsHole(player, lastSeedIndex) {
    // parameters: a string, player indicates who is the player,
    // an index -> indicates in which hole the player scattered his last seed
    // returns a boolean, true if we are on the opponents hole
    
    let authorized = false
    if (player == "top" && lastSeedIndex > 5) {
        authorized = true
    } else if (player == "bottom" && lastSeedIndex < 6) {
        authorized = true
    }
    return authorized;
}


export function allOpponentsEmpty(anArray, player) {

    let theyReEmpty = true
    anArray.forEach(function (element, index) {
        if (onOpponentsHole(player, index) && element != 0) {
            theyReEmpty = false
            return theyReEmpty;
        }
    });
    return theyReEmpty;
}

export function canGive(anElement, anIndex) {

    let toOpponentsSide = 6 - (anIndex % 6)
    let possible = false

    if (anElement >= toOpponentsSide) {
        possible = true
    }
    return possible
}

export function canGiveFullTest(anArray, player) {
    let possible = true
    let possibleIndicator = 0

    anArray.forEach(function (element, index) {
        if ((player == "top" && index <= 5) || (player == "bottom" && index >= 6)) {

            if (!canGive(element, index)) {
                possibleIndicator += 1
            }
        }
    });
    if (possibleIndicator == 6) {
        possible = false
    }
    return possible
}


export function winner(player) {
    // returns the winner "top" or "bottom" or "none" if no one won yet

    let reserveTop = document.getElementById('top').innerHTML;
    let reserveBottom = document.getElementById('bottom').innerHTML;
    let winnerIs = "none"

    reserveTop = parseInt(reserveTop);
    reserveBottom = parseInt(reserveBottom);

    if (reserveTop > 24) {
        winnnerIs = "top"
    } else if (reserveBottom > 24) {
        winnerIs = "bottom"
    }
    return winnerIs;
}

export function winnerByDefault(player) {
    let reserveTop = document.getElementById('top');
    let reserveBottom = document.getElementById('bottom');
    let winnerIs = "none"

    reserveTop = parseInt(reserveTop);
    reserveBottom = parseInt(reserveBottom);

    if (reserveTop > reserveBottom) {
        winnerIs = "top"
    } else if (reserveBottom > reserveTop) {
        winnerIs = "bottom"
    }
    return winnerIs;
}