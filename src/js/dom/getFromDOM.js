import {nbSeedUpdate} from "./updateDOM.js"
import {reserveUpdate} from "./updateDOM.js"
import {swapColor} from "./updateDOM"
import {swapCursor} from "./updateDOM"

import {authorizedSide} from "./../algo/validations.js"
import {nonEmptyHole} from "./../algo/validations.js"
import {allOpponentsEmpty} from "./../algo/validations.js"
import {canGive} from "./../algo/validations.js"
import {canGiveFullTest} from "./../algo/validations.js"
import {winner} from "./../algo/validations.js"
import {winnerByDefault} from "./../algo/validations.js"

import {createReserve} from "./../algo/moveSeeds"
import {emptyStartHole} from "./../algo/moveSeeds"
import {scatter} from "./../algo/moveSeeds"
import {takeSeeds} from "./../algo/moveSeeds"
import {animGive} from "./../algo/moveSeeds"
import {animTake} from "./../algo/moveSeeds"


function playerNamebot() { //nom joueur 1
  var name1 = prompt("Nom joueur 1");
  document.getElementById('namebot').innerHTML = "joueur 1 :" + name1;
}
function playerNametop() {
  var name2 = prompt("Nom joueur 2"); //nom joueur 2
  document.getElementById('nametop').innerHTML = "joueur 2 :" + name2;
}
playerNamebot();
playerNametop();
function seedsAr() {
  // return a table containing the number of seeds in each hole,
  // the index of the table matches the playing direction
  let holes = document.getElementsByClassName('hole')
  let tabSeeds = []
  for(let i=0; i < holes.length; i++) {
    let seeds = holes[i].getAttribute('data-nbSeed')
    seeds = parseInt(seeds);
    tabSeeds.push(seeds);
  }
  return tabSeeds
}
let theIndexOne = 0
let daPlayer = "top"




const bigboardgame = document.querySelector('.boardgame');

bigboardgame.addEventListener('click', ({target}) => {
  if (!target.matches('.hole')) return;
  
  let holes = document.getElementsByClassName('hole');
  holes = Array.from(holes);
  theIndexOne = holes.indexOf(target);
  
  let holesHTMLColl = document.getElementsByClassName('hole');
  if (!authorizedSide(theIndexOne, daPlayer)) {
    animTake(holesHTMLColl[theIndexOne]);
    return
  }
  let theArray = seedsAr()
  
  if (!nonEmptyHole(theArray, theIndexOne)) {
    return
  }
  if (allOpponentsEmpty(theArray, daPlayer)) {
    if (canGiveFullTest(theArray, daPlayer))   {
      if (!canGive(theArray[theIndexOne], theIndexOne)) {
        return
      }
    } else {
      if (winnerByDefault(daPlayer) == "top") {
        alert('Top player wins !');
      } else {
        alert('Bottom player wins');
      }
    }
  }
  
  let theReserve = createReserve(theArray, theIndexOne);
  theArray = emptyStartHole(theArray, theIndexOne);
  
  let theLastIndex = (theIndexOne + theReserve) % 12
  
  theArray = scatter(theArray, theReserve, theIndexOne, holesHTMLColl);
  

  nbSeedUpdate(theArray);
  
  let array2elems = takeSeeds(theLastIndex, theArray, daPlayer, holesHTMLColl);
  theArray = array2elems[0]
  let grenier = array2elems[1]
  nbSeedUpdate(theArray);

  reserveUpdate(grenier, daPlayer);

  if (winner(daPlayer) == "top") {
    alert('Top player wins !');
  } else if (winner(daPlayer) == "bottom") {
    alert('Bottom player wins !');
  }

  if (daPlayer == "top") {
    daPlayer = "bottom"
  } else {
    daPlayer = "top"
  }
  swapCursor(daPlayer);
  swapColor(daPlayer);
  return
});